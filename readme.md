
##Изучение Laravel на примере разработки блога

## php artisan

   <p>php artisan make:migration create_categoeies_table --create=categories создание миграции</p>
   <p>php artisan make:model Post -m создание модели и миграции одновременно</p>
    php artisan make:controller Admin\DashboardController - создание контроллера для routes
    php artisan tinker - интерактивная консоль php
    factory(App\Category::class)->create();
    
    php artisan vendor:publish --tag=laravel-pagination скопировать view допустим пагинации
    
    php artisan make:middleware [name] - создаем миддлвэр
    
    //Сначала заходим в middleware и там мутим все, потом страница грузится, прописывается в роутах
    
    make:mail создает почтовика
    
## Установка шаблона |
    
## Routes

routes/web.php прописываем Route::get('/admin', 'Admin\DashboardController@index' );
<br>в шаблоне подключаем собранные скрипты и стили
<br>в контроллере возвращаем view в методе Index
 <br> Создаем layout, вставляем туда все футеры и тд, без главного контента, вместо него @yield('content'),
 а его суем в шаблон
 <br> в шаблоне прописываем @extends('admin.layout')
  @section('content') ... @endsection
 
 
 ##|
    
 ## Пример выввода из базы в шаблоне
 
 @foreach($categories as $category)
                             <tr>
                                 <td>{{$category->id}}</td>
                                 <td>{{$category->title}}</td>
                                 <td><a href="edit.html" class="fa fa-pencil"></a> <a href="#" class="fa fa-remove"></a></td>
                             </tr>
                         @endforeach   
                         
  ##Ссылка в шаблоне через route
  
  {{route('categories.create')}}
  
  {{dd($errors->any())}} проверка ошибок
  
  {!! !!} вывод с рендером в html
  
  ##Forms
  
  {!! Form::open(['route' => ['categories.update', $category->id], 'method'=>'put'])!!} метод put
  ...
  {!! Form::close() !!}
  
  {{csrf_field()}} если просто html форма, это токен
    
## WebPack

npm run dev - сборка стилей и скриптов


diffForHumans() метод carbon показывающий разницу во времени

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of any modern web application framework, making it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 1100 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
