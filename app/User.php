<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use \Storage;

class User extends Authenticatable
{
    use Notifiable;


    const IS_BANNED = 1;
    const IS_ACTIVE = 0;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    //Добавление пользователя
    public static function add($fields)
    {
        $user = new static();
        $user->fill($fields);
        $user->save();

        return$user;
    }
    //Редактирование
    public function edit($fields)
    {
        $this->fill($fields);


        $this->save();
    }

    public function generatePassword($password)
    {
        if ($password != null) {
            $this->password = bcrypt($password);
            $this->save();
        }
    }

    public function removeAvatar()//Удаление аватара с проверочкой есть ли он
    {
        if($this->avatar != null)
        {
            Storage::delete('uploads/' . $this->avatar);//Удаление предыдущей картинки
        }
    }

    //Удаление
    public function remove()
    {
        $this->removeAvatar();//Удаление предыдущей картинки
        $this->delete();
    }
    //Загрузка аватара
    public function uploadAvatar($image)//Загрузка картинки
    {
        if($image == null) {return;}

        $this->removeAvatar();

        $filename = str_random(10) . '.' . $image->extension();//Генерация названия файла
        $image->storeAs('uploads', $filename);//saveAs(Папка, имя файла)
        $this->avatar = $filename;
        $this->save();
    }

    //Вывод аватара
    public function getAvatar()
    {
        if ($this->avatar == null)
        {
            return '/img/Image-Coming-Soon.png';//Дефолтная картинка
        }
        return '/uploads/' . $this->avatar;//Загруженная картинка
    }

    //Назначение админом
    public function makeAdmin()
    {
        $this->is_admin = 1;
        $this->save();
    }
    //Дроп админки
    public function makeNormal()
    {
        $this->is_admin = 0;
        $this->save();
    }

    public function toggleAdmin()
    {
        if ($this->is_admin == 1)
        {
            return $this->makeNormal();
        }

            return $this->makeAdmin();
    }

    //Банхамер
    public function ban()
    {
        $this->status = User::IS_BANNED;
        $this->save();
    }

    public function unban()
    {
        $this->status = User::IS_ACTIVE;
        $this->save();
    }

    public function toggleBan()
    {
        if ($this->status == 1)
        {
            return $this->unban();
        }

        return $this->ban();
    }

    public function currentUser()
    {
        return Auth::user()->id;
    }

    public static function checkBan()
    {
        if (Auth::user())
            return Auth::user()->status;
        else return 1;
    }
}
