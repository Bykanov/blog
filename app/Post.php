<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class Post extends Model
{
    use Sluggable;

    const IS_DRAFT = 0;
    const IS_PUBLIC = 1;
    //Поля для массового присвоения в add
    protected $fillable = ['title', 'content', 'date', 'description'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]//Название переделывает в ссылку на латинице
            //тип привет - privet
        ];
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
    public function category()
    {
        return $this->belongsTo(Category::class);//тип принадлежит категории
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');//user_id указываем потому что название author
        //и будет искаться как author_id
    }

    public function tags()
    {
        return $this->belongsToMany(
            Tag::class,
            'post_tags',
            'post_id',
            'tag_id'
        );
    }

    public static function add($fields)//Добавить статью
    {
        $post = new static;
        /*fill заполняет массив беря те title и content*/
        $post->fill($fields);
        $post->user_id = Auth::user()->id;//Запись айдишника
        //$post->title; //Заголовок
        $post->save();

        return $post;
    }

    public function edit($fields)//Изменить
    {
        $this->fill($fields);
        $this->save();
    }

    public function removeImage()//Удаление аватара с проверочкой есть ли он
    {
        if($this->image != null)
        {

            Storage::delete('uploads/' . $this->image);//Удаление предыдущей картинки
        }
    }

    public function remove()//Удалить
    {
        // удалить картинку поста
        $this->removeImage();
        $this->delete();
    }


    public function uploadImage($image)//Загрузка картинки
    {
        if($image == null) {return;}

        $this->removeImage();
        $filename = str_random(10) . '.' . $image->extension();//Генерация названия файла
        $image->storeAs('uploads', $filename);//saveAs(Папка, имя файла)
        $this->image = $filename;
        $this->save();
    }

    //Вывод картинки
    public function getImage()
    {
        if ($this->image == null)
        {
            return '/img/no-image.png';//Дефолтная картинка
        }
        return '/uploads/' . $this->image;//Загруженная картинка
    }
    //Привязка к категории
    public function setCategory($id)
    {
        if($id==null) {return;}

        $this->category_id = $id;//Не по ларавеловски
        $this->save();
    }

    //Привязка к тегам
    public function setTags($ids)
    {
        if($ids==null) {return;}

        $this->tags()->sync($ids);//Сохранение тегов
    }

    //Установка статуса статьи
    //Черновик
    public function setDraft()
    {
        $this->status = Post::IS_DRAFT;//Можно так через константы, а можно просто 0
        $this->save();
    }

    //Доступная
    public function setPublic()
    {
        $this->status = Post::IS_PUBLIC;
        $this->save();
    }

    //Переключатель статуса
    public function toggleStatus($value)
    {
        if ($value==null)
        {
           return $this->setDraft();
        }

            return $this->setPublic();
    }

    //Рекомендованная статья
    public function setFeatured()
    {
        $this->is_featured = 1;
        $this->save();
    }

    //Обычная
    public function setStandart()
    {
        $this->is_featured = 0;
        $this->save();
    }

    //Переключатель статуса рекомендации
    public function toggleFeatured($value)
    {
        if ($value==null)
        {
            return $this->setStandart();
        }

        return $this->setFeatured();
    }

    //Преобразование формата даты Carbon (мутатор/сетер)
    public function setDateAttribute($value)
    {
        $date = Carbon::createFromFormat('d/m/y', $value)->format('Y-m-d');
        $this->attributes['date'] = $date;
    }

    //гетер
    public function getDateAttribute($value)
    {
       $date = Carbon::createFromFormat('Y-m-d', $value)->format('d/m/y');
       return $date;
    }

    public function getCategoryTitle()//вывод категории с проверкой на нулл
    {
      /*по простому
       *   if ($this->category != null)
        {
            return $this->category->title;
        }
        return 'Нет категории';*/

      //Для погромистов
        return ($this->category != null) ? $this->category->title : 'Нет категории';

    }

    public function getTagsTitles()
    {
        if (!$this->tags->isEmpty()) {
            return implode(', ', $this->tags->pluck('title')->all());
        }

        return 'Нет тегов';
    }

    public function getCategoryID()
    {
        return ($this->category != null) ? $this->category->id : null;
    }

    public function getDate()//Вывод даты на главную страницу
    {
        return Carbon::createFromFormat('d/m/y', $this->date)->format('F d, Y');
    }

    public function hasPrevious()
    {
        return self::where('id', '<', $this->id)->max('id');//Если сейчас допустим id 5, то выбираем максимальное из меньших
    }

    public function getPrevious()
    {
        $postID = $this->hasPrevious();//id предыдущего
        return self::find($postID);
    }

    public function hasNext()
    {
        return self::where('id', '>', $this->id)->min('id');//Если сейчас допустим id 5, то выбираем МИНИМАЛЬНО из Больших
    }

    public function getNext()
    {
        $postID = $this->hasNext();//
        return self::find($postID);
    }

    public function related()
    {
        return self::all()->except($this->id);//выводим все посты кроме текущего
    }

    public function hasCategory()
    {
        return $this->category !=null ? true : false;
    }

    public static function getPopularPosts()//запрос отдельным методом или в провайдере
    {
        return self::orderBy('views', 'desc')->take(3)->get();
    }

    public function getComments()
    {
        return $this->comments()->where('status', 1)->get();
    }
}
/*
$post = Post::find(1);
$post->category->title; //Название категории
$post->tags; //все тэги
$post->author->name;//имя из класса юзер*/