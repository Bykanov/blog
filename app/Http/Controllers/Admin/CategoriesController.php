<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    public function index()
    {

        $categories = Category::all();//Все категории из таблицы categories
        return view('admin.categories.index', ['categories' => $categories]);
    }

    public function create()
    {
        return view('admin.categories.create');
    }

    public function store(Request $request)//Request все данные с формы
    {
        $this->validate($request, [
            'title' => 'required' //Обзязательно к заполнению
        ]);
        Category::create($request->all());//вытаскивает данные с формы и записывает
        return redirect()->route('categories.index');
    }

    public function edit($id)
    {
        $category = Category::find($id);//поиск записи в базе
        return view('admin.categories.edit', ['category' =>$category]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required' //Обзязательно к заполнению
        ]);

        $category = Category::find($id);//Поиск
        $category->update($request->all());//Обновление в базе

        return redirect()->route('categories.index');
    }

    public function destroy($id)
    {
        Category::find($id)->delete();
        return redirect()->route('categories.index');
    }
}
