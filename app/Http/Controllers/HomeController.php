<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Tag;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        if (Auth::check())
        {
            //zaloginen
        }
        else
        {
            //guest
        }

        $posts = Post::where('status', 1)->paginate(2); //пагинация по 2 поста на странице потом на странице {{$posts->links()}}

        return view('pages.index', compact('posts', 'popularPosts', 'featuredPosts',
            'recentPosts', 'categories'));
    }

    public function show($slug)
    {
        $post = Post::where('slug', $slug)->firstOrFail();//firstOrFail ошибка 404
        $checkBan = User::checkBan();

        return view('pages.show', compact('post', 'checkBan'));
    }

    public function tag($slug)
    {
        $tag = Tag::where('slug', $slug)->firstOrFail();

        $posts = $tag->posts()->where('status', 1)->paginate(6);

        return view('pages.list', compact('posts'));
    }

    public function category($slug)
    {
        $category = Category::where('slug', $slug)->firstOrFail();

        $posts = $category->posts()->where('status', 1)->paginate(2);

        return view('pages.list', compact('posts'));
    }




}
