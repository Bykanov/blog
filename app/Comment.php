<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    //Разрешить комментарий
    public function allow()
    {
        $this->status = 1;
        $this->save();
    }

    //Запретить
    public function disAllow()
    {
        $this->status = 0;
        $this->save();
    }

    //Переключатель
    public function toggleStatus()
    {
        if ($this->status == 0)
        {
            return $this->allow();
        }

        return $this->disAllow();
    }

    //Удаление комментария
    public function remove()
    {
        $this->delete();
    }
}
